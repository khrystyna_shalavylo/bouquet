package mobiles.mobiles.impl;

import mobiles.mobiles.Bouquet;

public class FuneralBouquet implements Bouquet {

    private int price;

    public FuneralBouquet() {
        price = 90;
    }

    public int getPrice() {
        return price;
    }


    @Override
    public void createDefault(String flower) {
        System.out.println("FuneralBouquet created with " + flower);
    }

    @Override
    public void decorate(String decor, int currentPrice) {
        System.out.print(decor + "(" + currentPrice + ")");
    }

}
