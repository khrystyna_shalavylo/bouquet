package mobiles.mobiles.impl;


import mobiles.mobiles.Bouquet;

public class WeddingBouquet implements Bouquet {

    private int price;

    public WeddingBouquet(){
        price=100;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public void createDefault(String flower) {
        System.out.println("WeddingBouquet created with " + flower);
    }

    @Override
    public void decorate(String decor,int currentPrice) {
        System.out.print(decor+"("+currentPrice+")");
    }

}
