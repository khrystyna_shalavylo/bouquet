package mobiles.mobiles.impl;

import mobiles.mobiles.Bouquet;

public class SimpleBouquet implements Bouquet {

    private int price;

    public SimpleBouquet() {
        price = 80;
    }

    @Override
    public void   createDefault(String flower) {
        System.out.println("SimpleBouquet created with "+flower);
    }

    @Override
    public void decorate(String decor, int currentPrice) {
        System.out.print(decor+"("+currentPrice+")");
    }

    public int getPrice(){
        return price;
    }

}
