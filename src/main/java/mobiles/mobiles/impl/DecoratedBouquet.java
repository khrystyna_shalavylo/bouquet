package mobiles.mobiles.impl;

import mobiles.mobiles.Bouquet;

public class DecoratedBouquet extends Decorator {
    private int price;

    public DecoratedBouquet(Bouquet bouquetDecorator) {
        super(bouquetDecorator);
        price=bouquetDecorator.getPrice();
    }

    @Override
    public void decorate(String decor, int currentPrice){
        System.out.print(" + ");
        super.decorate(decor, currentPrice);
        price+=currentPrice;
    }

    public int getPrice(){
        return price;
    }
}
