package mobiles.mobiles.impl;

import mobiles.mobiles.Bouquet;

public class ValentineBouquet implements Bouquet {

    private int price;

    public ValentineBouquet() {
        price = 70;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public void createDefault(String flower) {
        System.out.println("ValentineBouquet created with " + flower);
    }

    @Override
    public void decorate(String decor, int currentPrice) {
        System.out.print(decor+"("+currentPrice+")");
    }

}
