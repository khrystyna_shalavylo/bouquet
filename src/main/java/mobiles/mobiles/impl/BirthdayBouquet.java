package mobiles.mobiles.impl;

import mobiles.mobiles.Bouquet;

public class BirthdayBouquet implements Bouquet {

    private int price;

    public BirthdayBouquet() {
        price = 60;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public void createDefault(String flower) {
        System.out.println("BirthdayBouquet created with " + flower);
    }

    @Override
    public void decorate(String decor, int currentPrice) {
        System.out.print(decor + "(" + currentPrice+ ")");
    }

}


