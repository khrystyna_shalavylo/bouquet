package mobiles.mobiles.impl;

import mobiles.mobiles.Bouquet;

public abstract class Decorator implements Bouquet {
    private Bouquet bouquetDecorator;

    public Decorator(Bouquet bouquet) {
        this.bouquetDecorator = bouquet;
    }

    public Bouquet getBouquetDecorator() {
        return bouquetDecorator;
    }

    @Override
    public void decorate(String decor, int currentPrice) {
        bouquetDecorator.decorate(decor, currentPrice);
    }
}
