package mobiles.mobiles;

public interface Bouquet {
    void decorate(String decor, int currentPrice);

    default void createDefault(String flovers) {
    }

    int getPrice();
}
