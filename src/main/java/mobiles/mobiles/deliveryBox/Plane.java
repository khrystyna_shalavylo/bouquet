package mobiles.mobiles.deliveryBox;

public class Plane implements DeliveryBox {
    @Override
    public int  deliveryBox(String city){
        System.out.println("Delivery by plane to "+city);
        System.out.println("Boxed in case.");
        return Price.PLANE;
    }

//    @Override
//    public int deliveryPrice(){
//        return Price.PLANE;
//    }

}
