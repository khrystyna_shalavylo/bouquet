package mobiles.mobiles.deliveryBox;

public class Context {
    private DeliveryBox deliveryBox;

    public void setDeliveryBox(DeliveryBox deliveryBox){
        this.deliveryBox=deliveryBox;
    }
    public int executeDeliveryBox(String city){
        return deliveryBox.deliveryBox(city);
    }

}
