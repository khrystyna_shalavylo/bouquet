package mobiles.factory;

import mobiles.BouquetType;
import mobiles.mobiles.Bouquet;

public abstract class BouquetStore {

    protected abstract Bouquet createBouquet(BouquetType type, String defaultBouquet);

    public Bouquet assemble(BouquetType type, String defaultBouquet) {
        Bouquet bouquet = createBouquet(type,defaultBouquet);
        bouquet.createDefault(defaultBouquet);
//        bouquet.box();
//        bouquet.delivery();
        bouquet.getPrice();
        return bouquet;
    }
}
