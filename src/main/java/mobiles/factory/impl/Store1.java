package mobiles.factory.impl;

import mobiles.BouquetType;
import mobiles.factory.BouquetStore;
import mobiles.mobiles.Bouquet;
import mobiles.mobiles.impl.*;

public class Store1 extends BouquetStore {

    @Override
    protected Bouquet createBouquet(BouquetType type, String defaultBouquet) {
        Bouquet bouquet = null;

        if (type == BouquetType.BIRTHDAY) {
            bouquet = new BirthdayBouquet();
        } else if (type == BouquetType.VALENTINE) {
            bouquet = new ValentineBouquet();
        } else if (type == BouquetType.SIMPLE) {
            bouquet = new SimpleBouquet();


        }
        return bouquet;
    }
}
