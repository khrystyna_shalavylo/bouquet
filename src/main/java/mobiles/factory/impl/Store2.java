package mobiles.factory.impl;

import mobiles.BouquetType;
import mobiles.factory.BouquetStore;
import mobiles.mobiles.Bouquet;
import mobiles.mobiles.deliveryBox.Auto;
import mobiles.mobiles.deliveryBox.Context;
import mobiles.mobiles.deliveryBox.DeliveryBox;
import mobiles.mobiles.deliveryBox.Plane;
import mobiles.mobiles.impl.DecoratedBouquet;
import mobiles.mobiles.impl.FuneralBouquet;
import mobiles.mobiles.impl.SimpleBouquet;
import mobiles.mobiles.impl.WeddingBouquet;

import java.util.Scanner;

public class Store2 extends BouquetStore {

    private String city;
    private Scanner in = new Scanner(System.in);
    Context context=new Context();

    public Store2() {
        city = "Lviv";
    }

    @Override
    protected Bouquet createBouquet(BouquetType type, String defaultBouquet) {
        Bouquet bouquet = null;

        if (type == BouquetType.SIMPLE) {

            bouquet = new SimpleBouquet();
            bouquet.createDefault(defaultBouquet);
            bouquet = new DecoratedBouquet(new SimpleBouquet());
            bouquet.decorate("jds", 10);
            bouquet.decorate("fefef", 15);
            String yourCity = in.nextLine();
            if (city.equals(yourCity)) {
                context.setDeliveryBox(new Auto());
                System.out.println("\nTotal price: " + (bouquet.getPrice() + context.executeDeliveryBox(yourCity)));
            } else {
                context.setDeliveryBox(new Plane());
                System.out.println("\nTotal price: " + (bouquet.getPrice()+ context.executeDeliveryBox(yourCity)));
            }
        } else if (type == BouquetType.FUNERAL) {
            bouquet = new FuneralBouquet();
        } else if (type == BouquetType.WEDDING) {
            bouquet = new WeddingBouquet();
        }
        return bouquet;
    }
}
